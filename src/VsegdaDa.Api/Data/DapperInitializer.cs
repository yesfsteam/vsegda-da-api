﻿using Dapper;

namespace VsegdaDa.Api.Data
{
    public static class DapperInitializer
    {
        public static void ConfigureDapper()
        {
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }
    }
}