﻿using System;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using VsegdaDa.Api.Models.Inner;

namespace VsegdaDa.Api.Data
{
    public interface ICreditApplicationRepository
    {
        Task SaveCreditApplicationCheckRequest(ApplicationCheckModel model);
        Task UpdateCreditApplication(ApplicationStatusModel model);
       // Task<ApplicationModel> GetLastCheckRequest(Guid creditApplicationId);
       // Task<ApplicationModel> GetConfirmRequest(Guid creditApplicationId);
        Task SaveCreditApplicationConfirmRequest(ApplicationConfirmationModel model);
        Task<string> GetConfirmationLocationCity(string city);
        Task<string> GetConfirmationLocationRegion(string region);
    }

    public class CreditApplicationRepository : ICreditApplicationRepository
    {
        private readonly IConfiguration configuration;

        public CreditApplicationRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveCreditApplicationCheckRequest(ApplicationCheckModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.VsegdaDaResultCode,
                model.VsegdaDaApplicationId,
                model.VsegdaDaApplicationBody,
                model.VsegdaDaBankUrl
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_check_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    vsegda_da_result_code,
                    vsegda_da_application_id,
                    vsegda_da_application_body,
                    vsegda_da_bank_url
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :vsegdaDaResultCode,
                    :vsegdaDaApplicationId,
                    CAST(:vsegdaDaApplicationBody AS json),
                    :vsegdaDaBankUrl
                );", queryParams);
        }
        
        public async Task UpdateCreditApplication(ApplicationStatusModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.Date,
                model.VsegdaDaApplicationBody,
                model.VsegdaDaProposalsBody,
                model.VsegdaDaOrderSum,
                model.VsegdaDaInitialPay
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                UPDATE credit_application_check_request
                SET
                    updated_date = :date,
                    vsegda_da_application_body = CAST(:vsegdaDaApplicationBody AS json),
                    vsegda_da_proposals_body = CAST(:vsegdaDaProposalsBody AS json),
                    vsegda_da_order_sum = :vsegdaDaOrderSum,
                    vsegda_da_initial_pay = :vsegdaDaInitialPay                    
                WHERE
                    credit_application_id = :creditApplicationId::uuid;", queryParams);
        }

        public async Task<ApplicationModel> GetLastCheckRequest(Guid creditApplicationId)
        {
            await using var connection = createConnection();
            return await connection.QueryFirstOrDefaultAsync<ApplicationModel>($@"
                SELECT
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    vsegda_da_result_code,
                    vsegda_da_application_id,
                    vsegda_da_application_body,
                    vsegda_da_bank_url,
                    vsegda_da_order_num,
                    vsegda_da_status,
                    vsegda_da_proposals_body
                FROM credit_application_check_request
                WHERE
                    credit_application_id = :creditApplicationId::uuid
                ORDER BY
                    created_date DESC;", new {creditApplicationId});
        }

        public async Task SaveCreditApplicationConfirmRequest(ApplicationConfirmationModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.VsegdaDaApplicationId,
                model.VsegdaDaProposalId,
                model.VsegdaDaResultCode
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_confirm_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    vsegda_da_application_id,
                    vsegda_da_proposal_id,
                    vsegda_da_result_code
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :vsegdaDaApplicationId,
                    :vsegdaDaProposalId,
                    :vsegdaDaResultCode
                );", queryParams);
        }

        public async Task<string> GetConfirmationLocationCity(string city)
        {
            await using var connection = createConnection();
            return await connection.QueryFirstOrDefaultAsync<string>(@"
                SELECT
                    vsegdada_city
                FROM confirmation_location_city
                WHERE
                    kladr_city = :city;", new {city});
        }

        public async Task<string> GetConfirmationLocationRegion(string region)
        {
            await using var connection = createConnection();
            return await connection.QueryFirstOrDefaultAsync<string>(@"
                SELECT
                    vsegdada_region
                FROM confirmation_location_region
                WHERE
                    kladr_region = :region;", new {region});
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}