﻿using System;

namespace VsegdaDa.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToVsegdaDaDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
        
        public static string ToVsegdaDaDateFormat(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("yyyy-MM-dd") : string.Empty;
        }
    }
}