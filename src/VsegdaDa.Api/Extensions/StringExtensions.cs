﻿namespace VsegdaDa.Api.Extensions
{
    public static class StringExtensions
    {
        public static string ToVsegdaDaPhoneFormat(this string phone)
        {
            return string.IsNullOrWhiteSpace(phone) ? null : phone.Substring(1);
        }
    }
}