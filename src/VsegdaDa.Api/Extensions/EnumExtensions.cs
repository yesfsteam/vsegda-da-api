﻿using System;
using VsegdaDa.Api.Models.VsegdaDa.Enums;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace VsegdaDa.Api.Extensions
{
    public static class EnumExtensions
    {
        public static string ToVsegdaDaEducation(this Education? education)
        {
            if (!education.HasValue)
                return null;
            switch (education.Value)
            {
                case Education.PrimarySchool:
                case Education.HighSchool:
                case Education.SpecializedHighSchool:
                    return VsegdaDaEducation.Secondary.GetDescription();
                case Education.IncompleteSecondaryEducation:
                    return VsegdaDaEducation.IncompleteSecondary.GetDescription();
                case Education.IncompleteHigherEducation:
                    return VsegdaDaEducation.IncompleteHigher.GetDescription();
                case Education.HigherEducation:
                case Education.TwoOrMoreHigherEducations:
                    return VsegdaDaEducation.Higher.GetDescription();
                case Education.AcademicDegree:
                    return VsegdaDaEducation.Postgraduate.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(education)} value", nameof(education));
            }
        }
        
        public static string ToVsegdaDaMaritalStatus(this MaritalStatus? maritalStatus)
        {
            if (!maritalStatus.HasValue)
                return null;
            switch (maritalStatus.Value)
            {
                case MaritalStatus.Single:
                    return VsegdaDaMaritalStatus.Single.GetDescription();
                case MaritalStatus.Married:
                    return VsegdaDaMaritalStatus.Married.GetDescription();
                case MaritalStatus.Widowed:
                    return VsegdaDaMaritalStatus.Widowed.GetDescription();
                case MaritalStatus.Divorced:
                    return VsegdaDaMaritalStatus.Divorced.GetDescription();
                case MaritalStatus.Remarriage:
                    return VsegdaDaMaritalStatus.Married.GetDescription();
                case MaritalStatus.CivilMarriage:
                    return VsegdaDaMaritalStatus.DeFactoMarried.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(maritalStatus)} value", nameof(maritalStatus));
            }
        }
        
        public static string ToVsegdaDaConfirmationDocument(this ConfirmationDocument? confirmationDocument)
        {
            if (!confirmationDocument.HasValue)
                return null;
            switch (confirmationDocument.Value)
            {
                case ConfirmationDocument.IncomeStatement:
                    return VsegdaDaConfirmationDocument.IncomeStatement.GetDescription();
                case ConfirmationDocument.EmployerStampedIncomeStatement:
                    return VsegdaDaConfirmationDocument.EmployerStampedIncomeStatement.GetDescription();
                case ConfirmationDocument.BankStatement:
                    return VsegdaDaConfirmationDocument.BankStatement.GetDescription();
                case ConfirmationDocument.PensionFundBankStatement:
                    return VsegdaDaConfirmationDocument.PensionFundBankStatement.GetDescription();
                case ConfirmationDocument.NoDocument:
                    return VsegdaDaConfirmationDocument.NoDocument.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(confirmationDocument)} value", nameof(confirmationDocument));
            }
        }
        
        public static string ToVsegdaDaEmployerIndustry(this EmployerIndustry? employerIndustry)
        {
            if (!employerIndustry.HasValue)
                return null;
            switch (employerIndustry.Value)
            {
                case EmployerIndustry.Banks:
                    return VsegdaDaEmployerIndustry.Banks.GetDescription();
                case EmployerIndustry.CharityAndReligiousOrganizations:
                    return VsegdaDaEmployerIndustry.CharityAndReligiousOrganizations.GetDescription();
                case EmployerIndustry.Military:
                    return VsegdaDaEmployerIndustry.Military.GetDescription();
                case EmployerIndustry.CivilService:
                    return VsegdaDaEmployerIndustry.CivilService.GetDescription();
                case EmployerIndustry.Mining:
                    return VsegdaDaEmployerIndustry.Mining.GetDescription();
                case EmployerIndustry.Healthcare:
                    return VsegdaDaEmployerIndustry.Healthcare.GetDescription();
                case EmployerIndustry.Gambling:
                    return VsegdaDaEmployerIndustry.Gambling.GetDescription();
                case EmployerIndustry.InformationTechnology:
                    return VsegdaDaEmployerIndustry.InformationTechnology.GetDescription();
                case EmployerIndustry.ScienceEducation:
                    return VsegdaDaEmployerIndustry.ScienceEducation.GetDescription();
                case EmployerIndustry.HumanResources:
                    return VsegdaDaEmployerIndustry.HumanResources.GetDescription();
                case EmployerIndustry.Manufacturing:
                    return VsegdaDaEmployerIndustry.Manufacturing.GetDescription();
                case EmployerIndustry.Marketing:
                    return VsegdaDaEmployerIndustry.Marketing.GetDescription();
                case EmployerIndustry.Restaurants:
                    return VsegdaDaEmployerIndustry.Restaurants.GetDescription();
                case EmployerIndustry.Agriculture:
                    return VsegdaDaEmployerIndustry.Agriculture.GetDescription();
                case EmployerIndustry.Insurance:
                    return VsegdaDaEmployerIndustry.Insurance.GetDescription();
                case EmployerIndustry.Development:
                    return VsegdaDaEmployerIndustry.Development.GetDescription();
                case EmployerIndustry.Trading:
                    return VsegdaDaEmployerIndustry.Trading.GetDescription();
                case EmployerIndustry.Transport:
                    return VsegdaDaEmployerIndustry.Transport.GetDescription();
                case EmployerIndustry.Tourism:
                    return VsegdaDaEmployerIndustry.Tourism.GetDescription();
                case EmployerIndustry.LegalServices:
                    return VsegdaDaEmployerIndustry.LegalServices.GetDescription();
                case EmployerIndustry.Other:
                    return VsegdaDaEmployerIndustry.Other.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(employerIndustry)} value", nameof(employerIndustry));
            }
        }
        
        public static string ToVsegdaDaActivity(this Activity? activity)
        {
            if (!activity.HasValue)
                return null;
            switch (activity.Value)
            {
                case Activity.Employee:
                    return VsegdaDaActivity.Employee.GetDescription();
                case Activity.SelfEmployed:
                    return VsegdaDaActivity.SelfEmployed.GetDescription();
                case Activity.Unemployed:
                    return VsegdaDaActivity.Unemployed.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(activity)} value", nameof(activity));
            }
        }
        
        public static string ToVsegdaDaEmployeePosition(this EmployeePosition? employeePosition)
        {
            if (!employeePosition.HasValue)
                return null;
            switch (employeePosition)
            {
                case EmployeePosition.Employee:
                    return VsegdaDaEmployeePosition.Employee.GetDescription();
                case EmployeePosition.Specialist:
                    return VsegdaDaEmployeePosition.Specialist.GetDescription();
                case EmployeePosition.Manager:
                    return VsegdaDaEmployeePosition.Manager.GetDescription();
                case EmployeePosition.Head:
                    return VsegdaDaEmployeePosition.Head.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(employeePosition)} value", nameof(employeePosition));
            }
        }
        
        public static string ToVsegdaDaEmployeePositionType(this EmployeePosition? employeePosition)
        {
            if (!employeePosition.HasValue)
                return null;
            switch (employeePosition)
            {
                case EmployeePosition.Employee:
                    return VsegdaDaEmployeePositionType.Employee.GetDescription();
                case EmployeePosition.Specialist:
                    return VsegdaDaEmployeePositionType.Specialist.GetDescription();
                case EmployeePosition.Manager:
                    return VsegdaDaEmployeePositionType.Manager.GetDescription();
                case EmployeePosition.Head:
                    return VsegdaDaEmployeePositionType.Head.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(employeePosition)} value", nameof(employeePosition));
            }
        }
    }
}