﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using VsegdaDa.Api.Models;
using VsegdaDa.Api.Models.Configuration;
using VsegdaDa.Api.Models.Inner;
using VsegdaDa.Api.Models.VsegdaDa;
using VsegdaDa.Api.Models.VsegdaDa.Authentication;
using Yes.Infrastructure.Http;

namespace VsegdaDa.Api.Domain
{
    public interface IVsegdaDaClient
    {
        Task<Response<CreateApplicationResponse>> CreateApplication(CreateApplicationRequest request);
        Task<Response<GetApplicationResponse>> GetApplication(Guid applicationId);
        Task<Response<ConfirmApplicationResponse>> ConfirmApplication(ConfirmApplicationRequestModel model);
    }

    public class VsegdaDaClient : IVsegdaDaClient
    {
        private readonly VsegdaDaApiConfiguration configuration;
        private readonly ILogger<VsegdaDaClient> logger;
        private volatile HttpClient httpClient;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        
        public VsegdaDaClient(ILogger<VsegdaDaClient> logger, VsegdaDaApiConfiguration configuration, HttpClient httpClient)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.httpClient = httpClient;
            this.httpClient.Timeout = configuration.Timeout; 
            jsonSerializerSettings = new JsonSerializerSettings{ContractResolver = new CamelCasePropertyNamesContractResolver()};
        }

        public async Task<Response<CreateApplicationResponse>> CreateApplication(CreateApplicationRequest request)
        {
            return await call(request, createApplication);
        }

        public async Task<Response<GetApplicationResponse>> GetApplication(Guid applicationId)
        {
            return await call(applicationId, getApplication);
        }

        public async Task<Response<ConfirmApplicationResponse>> ConfirmApplication(ConfirmApplicationRequestModel model)
        {
            return await call(model, confirmApplication);
        }

        private async Task<Response<CreateApplicationResponse>> createApplication(CreateApplicationRequest request)
        {
            return await post<CreateApplicationResponse>("open-api-application/v1/applications", request);
        } 
        
        private async Task<Response<GetApplicationResponse>> getApplication(Guid applicationId)
        {
            return await get<GetApplicationResponse>($"open-api-application/v1/applications/{applicationId}");
        } 
        
        private async Task<Response<ConfirmApplicationResponse>> confirmApplication(ConfirmApplicationRequestModel model)
        {
            return await patch<ConfirmApplicationResponse>($"open-api-application/v1/applications/{model.AddplicationId}/decisions/{model.GatewayId}", model.Request);
        } 
        
        private async Task<Response<T>> post<T>(string uri, object obj)
        {
            var requestUri = new Uri(new Uri(configuration.Url), uri);
            var content = new JsonContent(obj, jsonSerializerSettings);
            var response = await httpClient.PostAsync(requestUri, content);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> get<T>(string uri)
        {
            var requestUri = new Uri(new Uri(configuration.Url), uri);
            var response = await httpClient.GetAsync(requestUri);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> patch<T>(string uri, object obj)
        {
            var requestUri = new Uri(new Uri(configuration.Url), uri);
            var content = new JsonContent(obj, jsonSerializerSettings);
            var response = await httpClient.PatchAsync(requestUri, content);
            return await createResponse<T>(response);
        }

        private async Task<Response<T>> createResponse<T>(HttpResponseMessage response)
        {
            var result = new Response<T>
            {
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                StatusCode = response.StatusCode
            };
            
            var content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                result.Content = JsonConvert.DeserializeObject<T>(content);
            else
                result.ErrorMessage = content;
            
            return result;
        }

        private async Task<Response<TResponse>> call<TRequest, TResponse>(TRequest request, Func<TRequest, Task<Response<TResponse>>> func)
        {
            if (httpClient.DefaultRequestHeaders.Authorization == null)
            {
                var tokenResponse = await refreshToken();
                if (!tokenResponse.IsSuccessStatusCode)
                    return Response<TResponse>.Unauthorized(tokenResponse.ErrorMessage);

                return await func(request);
            }
            else
            {
                var response = await func(request);
                if (response.StatusCode != HttpStatusCode.Unauthorized) 
                    return response;
            
                var tokenResponse = await refreshToken();
                if (!tokenResponse.IsSuccessStatusCode)
                    return Response<TResponse>.Unauthorized(tokenResponse.ErrorMessage);

                return await func(request);
            }
        }

        private async Task<Response> refreshToken()
        {
            var request = new GetTokenRequest
            {
                Username = configuration.Username,
                Password = configuration.Password,
                GrantType = Constants.GRANT_TYPE
            };
            
            var requestUri = new Uri(new Uri(configuration.AuthUrl), "open-api-authentication/v1/token");
            var content = new JsonContent(request, jsonSerializerSettings);
            httpClient.DefaultRequestHeaders.Authorization = null;//если не обнулить, то вернется 401
            var response = await httpClient.PostAsync(requestUri, content);
            logger.LogInformation($"GetToken response status code: {response.StatusCode}");
            
            if (response.IsSuccessStatusCode)
            {
                var result = JsonConvert.DeserializeObject<GetTokenResponse>(await response.Content.ReadAsStringAsync());
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", result.Data.AccessToken);
                return Response.Ok();
            }

            return new Response<GetTokenResponse>
            {
                StatusCode = response.StatusCode,
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                ErrorMessage = await response.Content.ReadAsStringAsync()
            };
        }
    }
}