﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using SD.Messaging.Models;
using VsegdaDa.Api.Data;
using VsegdaDa.Api.Extensions;
using VsegdaDa.Api.Models;
using VsegdaDa.Api.Models.Commands;
using VsegdaDa.Api.Models.Configuration;
using VsegdaDa.Api.Models.Inner;
using VsegdaDa.Api.Models.VsegdaDa;
using VsegdaDa.Api.Models.VsegdaDa.Enums;
using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace VsegdaDa.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
        Task<ProcessingResult> GetCreditApplicationStatus(GetCreditApplicationStatusCommand command);
        Task<ProcessingResult> SelectDecision(SelectDecisionCommand command);
     //   Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, ConfirmCreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly IVsegdaDaClient client;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;
        private const string PENDING_DETAILS = "Запрос отправлен, ожидайте ответ от кредитной организации";
        private const string NO_RESPONSE_DETAILS = "Нет ответа по данному кредитору";
        private const string SERVER_UNEXPECTED_ERROR_DETAILS = "Детали ошибки в логах приложения";
        
        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            ICommandSender commandSender, IVsegdaDaClient client, ApplicationConfiguration configuration, 
            MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.client = client;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (@event.ProfileType == ProfileType.Short || @event.ProfileType == ProfileType.Medium)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, "Заявка не отправляется для короткой и средней анкеты");
                    logger.LogInformation($"CheckCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                
                var request = await createRequest(@event, @event.CreditApplicationId);
                var response = await client.CreateApplication(request);

                await repository.SaveCreditApplicationCheckRequest(new ApplicationCheckModel
                {
                    CreditApplicationId = @event.CreditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    VsegdaDaResultCode = response.Content?.ResultCode,
                    VsegdaDaApplicationId = response.Content?.Data?.Application?.Id,
                    VsegdaDaApplicationBody = response.Content?.Data?.Application?.ToJsonString(),
                    VsegdaDaBankUrl = response.Content?.BankUrl
                });
                
                if (response.IsSuccessStatusCode)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Pending, @event.CreditApplicationId, PENDING_DETAILS);
                    var statusCommand = publishGetCreditApplicationStatusCommand(@event.CreditApplicationId, response.Content.Data.Application.Id);
                    logger.LogInformation($"CheckCreditApplication. Create application request sent. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}, {nameof(GetCreditApplicationStatusCommand)}: {statusCommand}");
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogInformation($"CheckCreditApplication. Create application request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorCheck, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogError($"CheckCreditApplication. Create application request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }
               
                if (@event.RetryCount == messagingConfiguration.CreditApplicationCreatedEventMaxRetryAttempts - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, @event.CreditApplicationId,
                        $"Количество отправленных запросов: {messagingConfiguration.CreditApplicationCreatedEventMaxRetryAttempts}. Последняя ошибка: {response.ErrorMessage}");
                    logger.LogWarning($"CheckCreditApplication. Create application request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Max retry count ({messagingConfiguration.CreditApplicationCreatedEventMaxRetryAttempts}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                // Переповтор
                logger.LogWarning($"CheckCreditApplication. Create application request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}), try again later. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.CreditApplicationCreatedEventMaxRetryAttempts - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.CreditApplicationCreatedEventMaxRetryAttempts}. {SERVER_UNEXPECTED_ERROR_DETAILS}");
                    logger.LogError(e, $"CheckCreditApplication. Error while processing create application request. Max retry count ({messagingConfiguration.CreditApplicationCreatedEventMaxRetryAttempts}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"CheckCreditApplication. Error while processing create application request, try again later. Duration: {beginTime.GetDuration()} Event: {@event}");
                return ProcessingResult.Fail();
            }
        }

        public async Task<ProcessingResult> GetCreditApplicationStatus(GetCreditApplicationStatusCommand command)
        {
            var beginTime = DateTime.Now;
            var lastTry = command.RetryCount == messagingConfiguration.GetCreditApplicationStatusCommandMaxRetryAttempts - 1;
            try
            {
                var response = await client.GetApplication(command.VsegdaDaApplicationId);
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.Data;
                    await repository.UpdateCreditApplication(new ApplicationStatusModel
                    {
                        CreditApplicationId = command.CreditApplicationId,
                        Date = beginTime,
                        VsegdaDaApplicationBody = data.Application.ToJsonString(),
                        VsegdaDaProposalsBody = data.Proposals.ToJsonString(),
                        VsegdaDaOrderSum = data.OrderSum,
                        VsegdaDaInitialPay = data.InitialPay
                    });
                    
                    var creditApplicationDecisions = new List<CreditApplicationProposalStatusModel>();
                    foreach (var proposal in data.Proposals)
                    {
                        if (!configuration.CreditOrganizations.ContainsKey(proposal.Partner.ToString()))
                        {
                            logger.LogWarning($"Unknown partner: {proposal.Partner.GetDescription()} Proposal: {proposal}");
                            continue;
                        }

                        var statusModel = new CreditApplicationProposalStatusModel
                        {
                            CreditOrganizationId = configuration.CreditOrganizations[proposal.Partner.ToString()],
                            Proposal = proposal
                        };
                        switch (proposal.Status)
                        {
                            case VsegdaDaProposalStatus.NEED_CONFIRMATION:
                                statusModel.Status = CreditOrganizationRequestStatus.Approved;
                                break;
                            case VsegdaDaProposalStatus.REJECTED:
                                statusModel.Status = CreditOrganizationRequestStatus.Declined;
                                statusModel.Details = proposal.Status.GetDescription();
                                break;
                            default:
                                statusModel.Status = CreditOrganizationRequestStatus.ValidationErrorCheck;
                                statusModel.Details = proposal.Status.GetDescription();
                                break;
                        }
                        creditApplicationDecisions.Add(statusModel);
                    }

                    foreach (var creditOrganization in configuration.CreditOrganizations)
                    {
                        if (creditApplicationDecisions.All(x => x.CreditOrganizationId != creditOrganization.Value))
                        {
                            creditApplicationDecisions.Add(new CreditApplicationProposalStatusModel
                            {
                                CreditOrganizationId = creditOrganization.Value,
                                Status = CreditOrganizationRequestStatus.Pending,
                                Details = $"{NO_RESPONSE_DETAILS}. ApplicationId: {command.VsegdaDaApplicationId}"
                            });
                        }
                    }

                    if (creditApplicationDecisions.All(x => x.Status != CreditOrganizationRequestStatus.Pending) || lastTry)
                    {
                        var approvedDecisions = creditApplicationDecisions.Where(x => x.Status == CreditOrganizationRequestStatus.Approved);
                        foreach (var decision in approvedDecisions)
                        {
                            var selectDecisionCommand = new SelectDecisionCommand
                            {
                                CreditApplicationId = command.CreditApplicationId,
                                CreditOrganizationId = decision.CreditOrganizationId,
                                Proposal = decision.Proposal,
                                VsegdaDaApplicationId = command.VsegdaDaApplicationId
                            };
                            commandSender.SendCommand(selectDecisionCommand, BoundedContexts.VSEGDA_DA_API, VsegdaDaApiRoutes.CONFIRMATIONS);
                            logger.LogInformation($"GetCreditApplicationStatus. Application approved. Duration: {beginTime.GetDuration()} {nameof(GetCreditApplicationStatusCommand)}: {command}, Response: {response.Content}, {nameof(SelectDecisionCommand)}: {selectDecisionCommand}");
                        }

                        var notApprovedDecisions = creditApplicationDecisions.Where(x => x.Status != CreditOrganizationRequestStatus.Approved).Select(x => (CreditApplicationStatusModel)x).ToList();
                        if (notApprovedDecisions.Any())
                        {
                            var decisionCommand = publishDecisionCommand(beginTime, command.CreditApplicationId, notApprovedDecisions);
                            logger.LogInformation($"GetCreditApplicationStatus. Application not approved. Duration: {beginTime.GetDuration()} {nameof(GetCreditApplicationStatusCommand)}: {command}, Response: {response.Content}, {nameof(CreditApplicationDecisionsCommand)}: {decisionCommand}");
                        }

                        return ProcessingResult.Ok();
                    }
                    logger.LogInformation($"GetCreditApplicationStatus. Not all application's status received, try again later. Duration: {beginTime.GetDuration()} {nameof(GetCreditApplicationStatusCommand)}: {command}, Response: {response.Content}");
                    return ProcessingResult.Fail();
                }

                if (lastTry)
                {
                    var decisionCommand = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, command.CreditApplicationId, response.ErrorMessage);
                    logger.LogWarning($"GetCreditApplicationStatus. Failed to get credit application status. Max retry count {messagingConfiguration.GetCreditApplicationStatusCommandMaxRetryAttempts} exceeded, finish processing. Duration: {beginTime.GetDuration()} {nameof(GetCreditApplicationStatusCommand)}: {command}, Response: {response.Content}, {nameof(CreditApplicationDecisionsCommand)}: {decisionCommand}");
                }
                else
                {
                    logger.LogWarning($"GetCreditApplicationStatus. Failed to get credit application status, try again later. Duration: {beginTime.GetDuration()} RequestCommand: {command}, ErrorMessage: {response.ErrorMessage}, Attempt: {command.RetryCount + 1}");
                }
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (lastTry)
                {
                    var decisionCommand = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, command.CreditApplicationId, SERVER_UNEXPECTED_ERROR_DETAILS);
                    logger.LogError(e, $"GetCreditApplicationStatus. Error while getting credit application status. Max retry count {messagingConfiguration.GetCreditApplicationStatusCommandMaxRetryAttempts} exceeded, finish processing. Duration: {beginTime.GetDuration()} {nameof(GetCreditApplicationStatusCommand)}: {command}, {nameof(CreditApplicationDecisionsCommand)}: {decisionCommand}");
                }
                else
                {
                    logger.LogError(e, $"GetCreditApplicationStatus. Error while getting credit application status, try again later. Duration: {beginTime.GetDuration()} {nameof(GetCreditApplicationStatusCommand)}: {command}");
                }
                return ProcessingResult.Fail();
            }
        }

        public async Task<ProcessingResult> SelectDecision(SelectDecisionCommand command)
        {
            var beginTime = DateTime.Now;
            var lastTry = command.RetryCount == messagingConfiguration.SelectDecisionCommandMaxRetryAttempts - 1;
            try
            {
                var request = new ConfirmApplicationRequestModel
                {
                    AddplicationId = command.VsegdaDaApplicationId,
                    GatewayId = command.Proposal.Id,
                    Request = new ConfirmApplicationRequest
                    {
                        SelectDecisionDto = new SelectDecisionDto
                        {
                            ConfirmationId = command.Proposal.BankConditions.Id
                        },
                        SignType = VsegdaDaSignType.Manual.GetDescription(),
                        SellerCode = configuration.VsegdaDaSellerCode,
                        SellerPlaceCode = configuration.VsegdaDaSellerPlaceCode
                    }
                };
                var response = await client.ConfirmApplication(request);

                await repository.SaveCreditApplicationConfirmRequest(new ApplicationConfirmationModel
                {
                    CreditApplicationId = command.CreditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    VsegdaDaApplicationId = command.VsegdaDaApplicationId,
                    VsegdaDaProposalId = command.Proposal.Id,
                    VsegdaDaResultCode = response.Content?.ResultCode
                });

                if (response.IsSuccessStatusCode)
                {
                    var decisionCommand = publishDecisionCommand(beginTime, command.CreditApplicationId,
                        new CreditApplicationStatusModel
                        {
                            Status = CreditOrganizationRequestStatus.Confirmed,
                            CreditOrganizationId = command.CreditOrganizationId,
                            CreditAmount = (int?) command.Proposal.BankConditions.Amount,
                            CreditPeriod = command.Proposal.BankConditions.PaymentNum,
                        });
                    logger.LogInformation($"SelectDecision. Success. Duration: {beginTime.GetDuration()} CreditApplicationId: {command.CreditApplicationId}, Request: {request}, Response: {response.Content}, {nameof(CreditApplicationDecisionsCommand)}: {decisionCommand}");
                    return ProcessingResult.Ok();
                }
                
                if (lastTry)
                {
                    var decisionCommand = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, command.CreditApplicationId, response.ErrorMessage);
                    logger.LogWarning($"SelectDecision. Select decision failed. Max retry count {messagingConfiguration.SelectDecisionCommandMaxRetryAttempts} exceeded, finish processing. Duration: {beginTime.GetDuration()} {nameof(SelectDecisionCommand)}: {command}, Response: {response.Content}, {nameof(CreditApplicationDecisionsCommand)}: {decisionCommand}");
                }
                else
                {
                    logger.LogWarning($"SelectDecision. Select decision failed, try again later. Duration: {beginTime.GetDuration()} RequestCommand: {command}, ErrorMessage: {response.ErrorMessage}, Attempt: {command.RetryCount + 1}");
                }
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (lastTry)
                {
                    var decisionCommand = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, command.CreditApplicationId, SERVER_UNEXPECTED_ERROR_DETAILS);
                    logger.LogError(e, $"SelectDecision. Error while selecting decision. Max retry count {messagingConfiguration.SelectDecisionCommandMaxRetryAttempts} exceeded, finish processing. Duration: {beginTime.GetDuration()} {nameof(SelectDecisionCommand)}: {command}, {nameof(CreditApplicationDecisionsCommand)}: {decisionCommand}");
                }
                else
                {
                    logger.LogError(e, $"SelectDecision. Error while selecting decision, try again later. Duration: {beginTime.GetDuration()} {nameof(GetCreditApplicationStatusCommand)}: {command}");
                }
                return ProcessingResult.Fail();
            }
        }

        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, ConfirmCreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (model.ProfileType == ProfileType.Short || model.ProfileType == ProfileType.Medium)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, "Заявка не отправляется для короткой и средней анкеты");
                }
                
                var request = await createRequest(model, creditApplicationId);
                var response = await client.CreateApplication(request);

                await repository.SaveCreditApplicationCheckRequest(new ApplicationCheckModel
                {
                    CreditApplicationId = creditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    VsegdaDaResultCode = response.Content?.ResultCode,
                    VsegdaDaApplicationId = response.Content?.Data?.Application?.Id,
                    VsegdaDaApplicationBody = response.Content?.Data?.Application?.ToJsonString(),
                    VsegdaDaBankUrl = response.Content?.BankUrl
                });
                
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    var statusCommand = publishGetCreditApplicationStatusCommand(creditApplicationId, response.Content.Data.Application.Id);
                    logger.LogInformation($"ConfirmCreditApplication. Create application request sent. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Model: {model}, {nameof(GetCreditApplicationStatusCommand)}: {statusCommand}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.Pending, PENDING_DETAILS);
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Create application request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, response.ErrorMessage);
                }
                
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Create application request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, response.ErrorMessage);
                }
                
                logger.LogInformation($"ConfirmCreditApplication. Create application request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ConfirmCreditApplication. Error while processing create application request. Duration: {beginTime.GetDuration()} Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }

        private async Task<CreateApplicationRequest> createRequest(CreditApplicationRequest request, Guid creditApplicationId)
        {
            var isResidenceAddressEmpty = request.IsResidenceAddressEmpty();
            var registrationAddress = new Address
            {
                InlineAddress = getRegistrationAddress(request),
                AddressKladr = new AddressKladr
                {
                    AddressFormat = Constants.ADDRESS_FORMAT,
                    KladrCode = request.RegistrationAddressKladrCode,
                    RegionCode = getRegionCode(request.RegistrationAddressRegionKladrCode),
                    RegionValue = request.RegistrationAddressRegion,
                    TownCode = getCityCode(request.RegistrationAddressCityKladrCode),
                    TownValue = request.RegistrationAddressCity,
                    StreetCode = getStreetCode(request.RegistrationAddressStreetKladrCode),
                    StreetValue = request.RegistrationAddressStreet,
                    HouseValue = request.RegistrationAddressHouse,
                    BlockValue = request.RegistrationAddressBlock,
                    BuildingValue = request.RegistrationAddressBuilding,
                    FlatValue = request.RegistrationAddressApartment
                }
            };
            return new CreateApplicationRequest
            {
                ClientInfo = new ClientInfo
                {
                    PersonalInfo = new PersonalInfo
                    {
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        MiddleName = request.MiddleName,
                        DateOfBirth = request.DateOfBirth.ToVsegdaDaDateFormat(),
                        PlaceOfBirth = request.PlaceOfBirth,
                        Sex = request.Gender.HasValue ? (request.Gender.Value == Gender.Male ? VsegdaDaSex.Male : VsegdaDaSex.Female).GetDescription() : null,
                        Income = request.MonthlyIncome?.ToString(),
                        Education = request.Education.ToVsegdaDaEducation(),
                        MaritalStatus = request.MaritalStatus.ToVsegdaDaMaritalStatus(),
                        DependantsCount = request.DependentsCount,
                        Properties = getProperties(request),
                        IncomeConfirmation = request.ConfirmationDocument.ToVsegdaDaConfirmationDocument()
                    },
                    Contacts = new Contact
                    {
                        Email = request.Email,
                        MobilePhone = request.PhoneNumber.ToVsegdaDaPhoneFormat()
                    },
                    RegistrationAddress = registrationAddress,
                    ResidentialAddress = isResidenceAddressEmpty
                        ? registrationAddress
                        : new Address
                        {
                            InlineAddress = getResidenceAddress(request),
                            AddressKladr = new AddressKladr
                            {
                                AddressFormat = Constants.ADDRESS_FORMAT,
                                KladrCode = request.ResidenceAddressKladrCode,
                                RegionCode = getRegionCode(request.ResidenceAddressRegionKladrCode),
                                RegionValue = request.ResidenceAddressRegion,
                                TownCode = getCityCode(request.ResidenceAddressCityKladrCode),
                                TownValue = request.ResidenceAddressCity,
                                StreetCode = getStreetCode(request.ResidenceAddressStreetKladrCode),
                                StreetValue = request.ResidenceAddressStreet,
                                HouseValue = request.ResidenceAddressHouse,
                                BlockValue = request.ResidenceAddressBlock,
                                BuildingValue = request.ResidenceAddressBuilding,
                                FlatValue = request.ResidenceAddressApartment
                            }
                        },
                    EmploymentAddress = new Address
                    {
                        InlineAddress = getEmployerAddress(request),
                        AddressKladr = new AddressKladr
                        {
                            AddressFormat = Constants.ADDRESS_FORMAT,
                            KladrCode = request.EmployerAddressKladrCode,
                            RegionCode = getRegionCode(request.EmployerAddressRegionKladrCode),
                            RegionValue = request.EmployerAddressRegion,
                            TownCode = getCityCode(request.EmployerAddressCityKladrCode),
                            TownValue = request.EmployerAddressCity,
                            StreetCode = getStreetCode(request.EmployerAddressStreetKladrCode),
                            StreetValue = request.EmployerAddressStreet,
                            HouseValue = request.EmployerAddressHouse,
                            BlockValue = request.EmployerAddressBlock,
                            BuildingValue = request.EmployerAddressBuilding,
                            FlatValue = request.EmployerAddressApartment
                        }
                    },
                    PassportInfo = new PassportInfo
                    {
                        PassportNumber = request.PassportSeries + request.PassportNumber,
                        CodeIssue = request.PassportDepartmentCode,
                        DateIssue = request.PassportIssueDate.ToVsegdaDaDateFormat(),
                        OrgIssue = request.PassportIssuer
                    },
                    Employment = new Employment
                    {
                        EmployedSince = request.EmployeeStartDate?.ToVsegdaDaDateFormat(),
                        EmployerTypeCode = request.EmployerIndustry.ToVsegdaDaEmployerIndustry(),
                        Inn = request.Tin,
                        OccupationTypeCode = request.Activity.ToVsegdaDaActivity(),
                        OrganizationName = request.EmployerName,
                        Phone = request.EmployerPhoneNumber.ToVsegdaDaPhoneFormat(),
                        Position = request.EmployeePosition.ToVsegdaDaEmployeePosition(),
                        PositionLevel = request.EmployeePosition.ToVsegdaDaEmployeePositionType(),
                        StaffCount = request.EmployerStaff
                    }
                },
                ApplicationInfo = new ApplicationInfo
                {
                    Category = Constants.CATEGORY,
                    Channel = Constants.CHANNEL,
                    ConfirmationLocation = await getConfirmationLocation(request)
                },
                Preferences = new Preference
                {
                    PaymentNum = request.CreditPeriod / 30 ?? 0,
                    Limit = request.CreditAmount
                },
                SellerCode = configuration.VsegdaDaSellerCode,
                SellerPlaceCode = configuration.VsegdaDaSellerPlaceCode,
                Consent = new Consent
                {
                    ConsentType = VsegdaDaConsentType.SmsConfirmedText.GetDescription(),
                    SessionId = creditApplicationId.ToString(),
                    SmsCode = request.ConfirmationCode,
                    ValidFrom = request.PersonalDataProcessApproveDate.ToVsegdaDaDateFormat()
                }
            };
        }

        private async Task<string> getConfirmationLocation(CreditApplicationRequest request)
        {
            var kladrCity = request.ResidenceAddressCity ?? request.RegistrationAddressCity;
            var city = await repository.GetConfirmationLocationCity(kladrCity);
            if (!string.IsNullOrWhiteSpace(city)) 
                return city;
            var region = await repository.GetConfirmationLocationRegion(request.ResidenceAddressRegion ?? request.RegistrationAddressRegion);
            return string.IsNullOrWhiteSpace(region) ? kladrCity : region;
        }

        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, CreditOrganizationRequestStatus status, Guid creditApplicationId, string details)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = configuration.CreditOrganizations.Select(x => new CreditApplicationStatusModel
                {
                    CreditOrganizationId = x.Value,
                    Status = status,
                    Details = details
                }).ToList()
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, Guid creditApplicationId, CreditApplicationStatusModel creditApplicationDecision)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    creditApplicationDecision
                }
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, Guid creditApplicationId, List<CreditApplicationStatusModel> creditApplicationDecisions)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = creditApplicationDecisions
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private GetCreditApplicationStatusCommand publishGetCreditApplicationStatusCommand(Guid creditApplicationId, Guid vsegdaDaApplicationId)
        {
            var command = new GetCreditApplicationStatusCommand
            {
                CreditApplicationId = creditApplicationId,
                VsegdaDaApplicationId = vsegdaDaApplicationId
            };
            commandSender.SendCommand(command, BoundedContexts.VSEGDA_DA_API, VsegdaDaApiRoutes.STATUSES);
            return command;
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, Guid creditOrganizationId, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = creditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            });
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = configuration.CreditOrganizations.Select(x => new CreditApplicationStatusModel
                {
                    CreditOrganizationId = x.Value,
                    Status = status,
                    Details = details
                }).ToList()
            });
        }
      
        private List<Property> getProperties(CreditApplicationRequest @event)
        {
            var result = new List<Property>();
            if (@event.HasFlat)
                result.Add(new Property {PropertyCode = VsegdaDaPropertyCode.Flat.GetDescription()});
            if (@event.HasHouse)
                result.Add(new Property {PropertyCode = VsegdaDaPropertyCode.House.GetDescription()});
            if (@event.HasArea)
                result.Add(new Property {PropertyCode = VsegdaDaPropertyCode.Area.GetDescription()});
            if (@event.HasCar)
                result.Add(new Property {PropertyCode = VsegdaDaPropertyCode.Car.GetDescription()});
            return result;
        }

        private string getRegistrationAddress(CreditApplicationRequest @event)
        {
            return $"{@event.RegistrationAddressRegion} {@event.RegistrationAddressCity} {@event.RegistrationAddressStreet} {@event.RegistrationAddressHouse} {@event.RegistrationAddressBlock} {@event.RegistrationAddressBuilding} {@event.RegistrationAddressApartment}";
        }
        
        private string getResidenceAddress(CreditApplicationRequest @event)
        {
            return $"{@event.ResidenceAddressRegion} {@event.ResidenceAddressCity} {@event.ResidenceAddressStreet} {@event.ResidenceAddressHouse} {@event.ResidenceAddressBlock} {@event.ResidenceAddressBuilding} {@event.ResidenceAddressApartment}";
        }
        
        private string getEmployerAddress(CreditApplicationRequest @event)
        {
            return $"{@event.EmployerAddressRegion} {@event.EmployerAddressCity} {@event.EmployerAddressStreet} {@event.EmployerAddressHouse} {@event.EmployerAddressBlock} {@event.EmployerAddressBuilding} {@event.EmployerAddressApartment}";
        }
        
        private string getRegionCode(string regionCodeKladr)
        {
            if (string.IsNullOrWhiteSpace(regionCodeKladr) || regionCodeKladr.Length < 2)
                return "00";
            return regionCodeKladr.Substring(0, 2);
        }
        
        private string getCityCode(string cityCodeKladr)
        {
            if (string.IsNullOrWhiteSpace(cityCodeKladr) || cityCodeKladr.Length < 8)
                return "000";
            return cityCodeKladr.Substring(5, 3);
        }
        
        private string getStreetCode(string streetCodeKladr)
        {
            if (string.IsNullOrWhiteSpace(streetCodeKladr) || streetCodeKladr.Length < 15)
                return "0000";
            return streetCodeKladr.Substring(11, 4);
        }
    }
}