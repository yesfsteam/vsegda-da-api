﻿using System;
using System.Collections.Generic;

namespace VsegdaDa.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public Dictionary<string, Guid> CreditOrganizations { get; set; }
        public string VsegdaDaSellerCode { get; set; }
        public string VsegdaDaSellerPlaceCode { get; set; }
    }
}