﻿using System;

namespace VsegdaDa.Api.Models.Configuration
{
    public class VsegdaDaApiConfiguration
    {
        public string AuthUrl { get; set; }
        public string Url { get; set; }
        public TimeSpan Timeout { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}