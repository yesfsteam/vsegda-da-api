﻿namespace VsegdaDa.Api.Models.Configuration
{
    public class MessagingConfiguration
    {
        public int CreditApplicationCreatedEventChannelsCount { get; set; }
        public int CreditApplicationCreatedEventMaxRetryAttempts { get; set; }
        public int GetCreditApplicationStatusCommandChannelsCount { get; set; }
        public int GetCreditApplicationStatusCommandMaxRetryAttempts { get; set; }
        public int SelectDecisionCommandChannelsCount { get; set; }
        public int SelectDecisionCommandMaxRetryAttempts { get; set; }
    }
}