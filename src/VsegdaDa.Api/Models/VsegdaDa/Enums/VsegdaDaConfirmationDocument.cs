﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaConfirmationDocument
    {
        [Description("SCAN_2NDFL")]
        IncomeStatement = 1,
        [Description("STAMPED_INCOME_STATEMENT")]
        EmployerStampedIncomeStatement = 2,
        [Description("BANK_STATEMENT")]
        BankStatement = 3,
        [Description("PFR_EXTRACT")]
        PensionFundBankStatement = 4,
        [Description("UNCONFIRMED")]
        NoDocument = 5
    }
}