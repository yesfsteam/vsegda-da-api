﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaActivity
    {
        [Description("COMMERCIAL_EMPLOYEE")]
        Employee = 1,
        [Description("SELF_EMPLOYED")]
        SelfEmployed = 2,
        [Description("UNEMPLOYED")]
        Unemployed = 3
    }
}