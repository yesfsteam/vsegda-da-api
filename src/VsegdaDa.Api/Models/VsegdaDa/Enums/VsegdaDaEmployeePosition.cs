﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaEmployeePosition
    {
        [Description("OMAY_Craftsman")]
        Employee = 1,
        [Description("OMAY_Specialist")]
        Specialist = 2,
        [Description("OMAY_Manager1")]
        Manager = 3,
        [Description("OMAY_Supervisor1")]
        Head = 4
    }
}