﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaEducation
    {
        [Description("SECONDARY")]
        Secondary = 1,
        [Description("HIGHER")]
        Higher = 2,
        [Description("INCOMPLETESECONDARY")]
        IncompleteSecondary = 3,
        [Description("INCOMPLETEHIGHER")]
        IncompleteHigher = 4,
        [Description("POSTGRADUATE")]
        Postgraduate = 5
    }
}