﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaPropertyCode
    {
        [Description("OMAY_FLAT")]
        Flat = 1,
        [Description("OMAY_HOUSE")]
        House = 2,
        [Description("OMAY_LAND")]
        Area = 3,
        [Description("OMAY_AUTO")]
        Car = 4
    }
}