﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaMaritalStatus
    {
        [Description("SINGLE")]
        Single = 1,
        [Description("MARRIED")]
        Married = 2,
        [Description("DIVORCED")]
        Divorced = 3,
        [Description("WIDOWED")]
        Widowed = 4,
        [Description("DEFACTOMARRIED")]
        DeFactoMarried = 5
    }
}