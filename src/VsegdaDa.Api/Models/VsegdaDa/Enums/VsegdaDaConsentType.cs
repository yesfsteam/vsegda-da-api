﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaConsentType
    {
        [Description("SMS_CONFIRMED_TEXT")]
        SmsConfirmedText = 1,
        [Description("CHECKBOX_CONFIRMED_TEXT")]
        CheckboxConfirmedText = 2,
        [Description("PARTNER_STORED")]
        PartnerStored  = 3
    }
}