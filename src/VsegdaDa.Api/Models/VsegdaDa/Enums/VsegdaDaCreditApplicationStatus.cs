﻿using System.ComponentModel;

// ReSharper disable InconsistentNaming

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaCreditApplicationStatus
    {
        [Description("Создано")]
        NEW = 1,
        [Description("На рассмотрении")]
        WAIT_APPROVE = 2,
        [Description("Рассмотрение завершено")]
        APPROVAL_PROCESS_FINISHED = 3,
        [Description("Партнер выбран")]
        PROVIDER_SELECTED = 4,
        [Description("Аннулировано")]
        CANCELLED = 5,
        [Description("Подписано")]
        SIGNED = 6,
        [Description("Ошибка")]
        ERROR = 7,
        [Description("Действующая")]
        ACTIVE = 8,
        [Description("Отказ")]
        REJECTED = 9
    }
}