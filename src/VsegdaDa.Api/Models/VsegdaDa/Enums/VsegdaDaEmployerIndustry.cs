﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaEmployerIndustry
    {
        [Description("BANKINGFINANCE")]
        Banks = 1,
        [Description("CHARITYRELIGION")]
        CharityAndReligiousOrganizations = 2,
        [Description("MILITARYINDUSTRY")]
        Military = 3,
        [Description("CIVILSERVICE")]
        CivilService = 4,
        [Description("MINING")]
        Mining = 5,
        [Description("HEALTHCARE")]
        Healthcare = 6,
        [Description("GAMBLING")]
        Gambling = 7,
        [Description("IT")]
        InformationTechnology = 8,
        [Description("SCIENCEEDUCATION")]
        ScienceEducation = 9,
        [Description("HR")]
        HumanResources = 10,
        [Description("MANUFACTURING")]
        Manufacturing = 11,
        [Description("PRMARKETING")]
        Marketing = 12,
        [Description("RESTAURANTS")]
        Restaurants = 13,
        [Description("AGRICULTURE")]
        Agriculture = 14,
        [Description("INSURANCE")]
        Insurance = 15,
        [Description("CONSTRUCTIONREALTY")]
        Development = 16,
        [Description("SALES")]
        Trading = 17,
        [Description("TRANSPORTLOGISTICS")]
        Transport = 18,
        [Description("TOURISMENTERTAINMENT")]
        Tourism = 19,
        [Description("LEGALSERVICES")]
        LegalServices = 20,
        [Description("OTHER")]
        Other = 21
    }
}