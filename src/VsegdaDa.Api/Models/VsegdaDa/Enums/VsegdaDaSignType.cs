﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaSignType
    {
        [Description("MANUAL")]
        Manual = 1,
        [Description("SMS_CODE")]
        SmsCode = 2,
        [Description("PARTNER_FRONT_SMS")]
        PartnerFrontSms  = 3
    }
}