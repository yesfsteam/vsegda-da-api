﻿using System.ComponentModel;

// ReSharper disable InconsistentNaming

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaPartner
    {
        [Description("АО \"Райффайзенбанк\"")]
        RFZB = 1,
        [Description("ООО \"ХКФ БАНК\"")]
        HCFB = 2
    }
}