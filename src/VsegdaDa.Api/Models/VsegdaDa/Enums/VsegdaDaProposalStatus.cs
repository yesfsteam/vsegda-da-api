﻿using System.ComponentModel;

// ReSharper disable InconsistentNaming

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaProposalStatus
    {
        [Description("На рассмотрении")]
        WAIT_APPROVE = 1,
        [Description("Нужны дополнительные данные")]
        NEED_MORE_DATA = 2,
        [Description("Нужны документы")]
        NEED_DOCUMENTS = 3,
        [Description("Ожидание подтверждения")]
        WAIT_CONFIRMATION = 4,
        [Description("По заявке ожидается выбор клиента")]
        NEED_CONFIRMATION = 5,
        [Description("Одобрено")]
        APPROVE = 6,
        [Description("Подписано")]
        SIGNED = 7,
        [Description("Действующее")]
        ACTIVE = 8,
        [Description("Отказано")]
        REJECTED = 9,
        [Description("Аннулировано")]
        CANCELLED = 10
    }
}