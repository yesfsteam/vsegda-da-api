﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaSex
    {
        [Description("MALE")]
        Male = 1,
        [Description("FEMALE")]
        Female = 2
    }
}