﻿using System.ComponentModel;

namespace VsegdaDa.Api.Models.VsegdaDa.Enums
{
    public enum VsegdaDaEmployeePositionType
    {
        [Description("SKILLED_WORKER")]
        Employee = 1,
        [Description("SPECIALIST")]
        Specialist = 2,
        [Description("MANAGEMENT_AVERAGE")]
        Manager = 3,
        [Description("HEAD")]
        Head = 4
    }
}