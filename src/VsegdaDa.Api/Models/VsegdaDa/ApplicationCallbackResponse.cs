﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using VsegdaDa.Api.Models.VsegdaDa.Enums;
using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa
{
    public class ApplicationCallbackResponse : JsonModel
    {
        public Guid AppId { get; set; }
        public string OrderNum { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public VsegdaDaCreditApplicationStatus Status { get; set; }
        public List<Proposal> Proposals { get; set; }
    }
}