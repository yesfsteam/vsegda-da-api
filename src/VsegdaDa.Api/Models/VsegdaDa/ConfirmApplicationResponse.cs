﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa
{
    public class ConfirmApplicationResponse : JsonModel
    {
        public string ResultCode { get; set; }
    }
}