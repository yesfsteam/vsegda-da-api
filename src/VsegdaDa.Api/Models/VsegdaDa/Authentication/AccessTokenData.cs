﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Authentication
{
    public class AccessTokenData : JsonModel
    {
        public string AccessToken { get; set; }
    }
}