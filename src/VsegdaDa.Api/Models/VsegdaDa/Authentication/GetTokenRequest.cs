﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Authentication
{
    public class GetTokenRequest : JsonModel
    {
        public string Username { get; set; }
        
        public string Password { get; set; }
        
        public string GrantType { get; set; }
    }
}