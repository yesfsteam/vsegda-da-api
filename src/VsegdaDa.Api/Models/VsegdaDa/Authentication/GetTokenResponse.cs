﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Authentication
{
    public class GetTokenResponse : JsonModel
    {
        public string ResultCode { get; set; }
        public AccessTokenData Data { get; set; }
    }
}