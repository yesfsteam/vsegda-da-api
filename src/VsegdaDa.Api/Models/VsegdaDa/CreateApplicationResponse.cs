﻿using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa
{
    public class CreateApplicationResponse : JsonModel
    {
        public string ResultCode { get; set; }
        public CreateApplicationResponseData Data { get; set; }
        public string BankUrl { get; set; }
    }
}