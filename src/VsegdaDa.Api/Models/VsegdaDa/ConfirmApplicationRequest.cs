﻿using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa
{
    public class ConfirmApplicationRequest : JsonModel
    {
        public SelectDecisionDto SelectDecisionDto { get; set; }
        public string SignType { get; set; }
        public string SellerCode { get; set; }
        public string SellerPlaceCode { get; set; }
    }
}