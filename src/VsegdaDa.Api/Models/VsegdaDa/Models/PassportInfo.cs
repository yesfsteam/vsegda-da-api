﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class PassportInfo : JsonModel
    {
        public string PassportNumber { get; set; }
        public string CodeIssue { get; set; }
        public string DateIssue { get; set; }
        public string OrgIssue { get; set; }
    }
}