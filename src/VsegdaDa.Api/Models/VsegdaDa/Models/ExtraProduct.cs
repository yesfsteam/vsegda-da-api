﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class ExtraProduct : JsonModel
    {
        public string ExtraProductCode { get; set; }
        public decimal? Sum { get; set; }
        public bool? IncludedInCredit { get; set; }
    }
    
    /* ExtraProductCode
        Код дополнительного продукта или услуги.
        Код в нотации «ВсегдаДа»
        Возможные значения:
        SMS – СМС-информирование
        SL – Страхование жизни
        SOPR – Страхование от потери работы
     */
}