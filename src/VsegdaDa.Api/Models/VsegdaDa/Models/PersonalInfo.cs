﻿using System.Collections.Generic;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class PersonalInfo : JsonModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Sex { get; set; }
        public string Income { get; set; }
        public string Education { get; set; }
        public string MaritalStatus { get; set; }
        public int? DependantsCount { get; set; }
        public List<Property> Properties { get; set; }
        public string IncomeConfirmation { get; set; }
    }
}