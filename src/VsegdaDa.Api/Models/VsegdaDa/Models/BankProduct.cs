﻿using VsegdaDa.Api.Models.VsegdaDa.Enums;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class BankProduct : JsonModel
    {
        public string Code { get; set; }
        public string PartnerProductCode { get; set; }
        public string Name { get; set; }
        public VsegdaDaPartner? Partner { get; set; }
        public decimal? AnnuityRate { get; set; }
        public decimal? MinLimit { get; set; }
        public decimal? MaxLimit { get; set; }
        public decimal? MinInitialPaymentAmount { get; set; }
        public decimal? MaxInitialPaymentAmount { get; set; }
        public decimal? MinInitialPaymentPercent { get; set; }
        public decimal? MaxInitialPaymentPercent { get; set; }
        public int? MinPaymentTerm { get; set; }
        public int? MaxPaymentTerm { get; set; }
    }
}