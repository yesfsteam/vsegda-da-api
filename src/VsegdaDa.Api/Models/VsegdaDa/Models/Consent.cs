﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Consent : JsonModel
    {
        public string ConsentType { get; set; }
        public string SessionId { get; set; }
        public string SmsCode { get; set; }
        public string ValidFrom { get; set; }
        //public string ValidTo { get; set; }
    }
}