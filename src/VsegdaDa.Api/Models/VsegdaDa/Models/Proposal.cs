﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using VsegdaDa.Api.Models.VsegdaDa.Enums;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Proposal : JsonModel
    {
        public Guid Id { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public VsegdaDaPartner Partner { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public VsegdaDaProposalStatus Status { get; set; }
        public BankConditions BankConditions { get; set; }
        public bool? Selected { get; set; }
        public string ProductCategory { get; set; }
        public List<RelatedProposals> RelatedProposals { get; set; }
        public List<Limits> Limits { get; set; }
        public bool? CardPaymentState { get; set; }
    }
}