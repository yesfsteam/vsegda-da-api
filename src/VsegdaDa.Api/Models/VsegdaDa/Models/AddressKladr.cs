﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class AddressKladr : JsonModel
    {
        public string AddressFormat { get; set; }
        public string KladrCode { get; set; }
        //public string Country { get; set; }
        public string RegionCode { get; set; }
        //public string RegionType { get; set; }
        public string RegionValue { get; set; }
        //public string DistrictCode { get; set; }
        //public string DistrictType { get; set; }
        //public string DistrictValue { get; set; }
        public string TownCode { get; set; }
        //public string TownType { get; set; }
        public string TownValue { get; set; }
        /*public string LocalityCode { get; set; }
        public string LocalityType { get; set; }
        public string LocalityValue { get; set; }*/
        public string StreetCode { get; set; }
        //public string StreetType { get; set; }
        public string StreetValue { get; set; }
        //public string HouseCode { get; set; }
        //public string HouseType { get; set; }
        public string HouseValue { get; set; }
        /*public string FlatCode { get; set; }
        public string FlatType { get; set; }*/
        public string FlatValue { get; set; }
        //public string BlockCode { get; set; }
        //public string BlockType { get; set; }
        public string BlockValue { get; set; }
        //public string BuildingCode { get; set; }
        //public string BuildingType { get; set; }
        public string BuildingValue { get; set; }
        //public string Zipcode { get; set; }
    }
}