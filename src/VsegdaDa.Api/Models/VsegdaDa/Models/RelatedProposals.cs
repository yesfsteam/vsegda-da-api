﻿using System;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class RelatedProposals
    {
        public Guid Id { get; set; }
        public string ProductCategory { get; set; }
    }
    /*  ProductCategory
        Код	    Значение
        POS	    POS заявка
        CASH	CASH заявка
        CARD	CARD заявка      
     */
}