﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class CreateApplicationResponseData : JsonModel
    {
        public Application Application { get; set; }
    }
}