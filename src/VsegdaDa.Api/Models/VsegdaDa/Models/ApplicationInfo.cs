﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class ApplicationInfo : JsonModel
    {
        public string Category { get; set; }
        public string Channel { get; set; }
        //public string OrderNum { get; set; }
        //public decimal? OrderSum { get; set; }
        //public string SellerStatus { get; set; }
        public string ConfirmationLocation { get; set; }
    }
}