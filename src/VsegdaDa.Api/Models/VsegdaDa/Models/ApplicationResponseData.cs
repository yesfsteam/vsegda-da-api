﻿using System.Collections.Generic;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class ApplicationResponseData : JsonModel
    {
        public decimal? OrderSum { get; set; }
        public decimal? InitialPay { get; set; }
        public Application Application { get; set; }
        public List<Proposal> Proposals { get; set; }
    }
}