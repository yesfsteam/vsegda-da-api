﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class SelectDecisionDto : JsonModel
    {
        public string ConfirmationId { get; set; } 
    }
}