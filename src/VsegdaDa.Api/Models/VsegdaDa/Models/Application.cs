﻿using System;
using Yes.Infrastructure.Common.Models;

// ReSharper disable UnusedMember.Global

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Application : JsonModel
    {
        public Guid Id { get; set; }
        public string ApplicationNum { get; set; }
        public string OrderNum { get; set; }
        public int? PaymentNum { get; set; }
        public string Category { get; set; }
        public string Channel { get; set; }
        public string Status { get; set; }
        public DateTime? SignedDate { get; set; }
    }
}