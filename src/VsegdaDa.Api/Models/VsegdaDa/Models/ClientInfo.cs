﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class ClientInfo : JsonModel
    {
        public PersonalInfo PersonalInfo { get; set; }
        public Contact Contacts { get; set; }
        //public AdditionalContact AdditionalContacts { get; set; }
        public Address RegistrationAddress { get; set; }
        public Address ResidentialAddress { get; set; }
        public Address EmploymentAddress { get; set; }
        public PassportInfo PassportInfo { get; set; }
        public Employment Employment { get; set; }
//        public Secret Secret { get; set; }
    }
}