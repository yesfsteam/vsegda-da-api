﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Contact : JsonModel
    {
        public string Email { get; set; }
        public string MobilePhone { get; set; }
    }
}