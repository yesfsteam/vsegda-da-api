﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Property : JsonModel
    {
        public string PropertyCode { get; set; }
    }
}