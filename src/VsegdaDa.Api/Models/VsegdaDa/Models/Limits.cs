﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Limits : JsonModel
    {
        public string PurchaseCategoryCode { get; set; }
        public decimal? BaseAmountMax { get; set; }
        public decimal? PaymentMax { get; set; }
    }
    /*  PurchaseCategoryCode
        Код	                Значение
        OMAY_Furniture	    Мебель
        OMAY_Clothing	    Одежда
        OMAY_MobileJewelry	Мобильная связь\Ювелирные изделия
        OMAY_Electronics	Бытовая техника
        OMAY_Universal	    Универсальный   
     */
}