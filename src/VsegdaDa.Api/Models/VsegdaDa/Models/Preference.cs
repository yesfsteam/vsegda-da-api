﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Preference : JsonModel
    {
        public decimal? InitialPay { get; set; }
        public int? PaymentNum { get; set; }
        public string ProductCode { get; set; }
        public decimal? Limit { get; set; }
        public string CategoryCode { get; set; }
    }
}