﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Employment : JsonModel
    {
        public string EmployedSince { get; set; }
        public string EmployerTypeCode { get; set; }
        public string Inn { get; set; }
        public string OccupationTypeCode { get; set; }
        public string OrganizationName { get; set; }
        public string Phone { get; set; }
        //public string PhoneExtension { get; set; }
        public string Position { get; set; }
        public string PositionLevel { get; set; }
        public int? StaffCount { get; set; }
    }
}