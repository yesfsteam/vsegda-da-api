﻿using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class Address : JsonModel
    {
        public string InlineAddress { get; set; }
        public AddressKladr AddressKladr { get; set; }
        //public string RegistrationDate { get; set; }
    }
}