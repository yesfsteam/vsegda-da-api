﻿using System.Collections.Generic;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa.Models
{
    public class BankConditions : JsonModel
    {
        public string Id { get; set; }//todo Guid ?
        public BankProduct BankProduct { get; set; }
        public decimal Amount { get; set; }
        public decimal AnnuityPay { get; set; }
        public int PaymentNum { get; set; }
        public decimal InitialPay { get; set; }
        public string AccountNumber { get; set; }
        public string ContractNumber { get; set; }
        public List<ExtraProduct> ExtraProducts { get; set; }
        public decimal? ExtraTotalSumInCredit { get; set; }
        public decimal? ExtraTotalSumOutOfCredit { get; set; }
    }
}