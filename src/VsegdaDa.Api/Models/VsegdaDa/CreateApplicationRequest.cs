﻿using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa
{
    public class CreateApplicationRequest : JsonModel
    {
        public ClientInfo ClientInfo { get; set; }
        public ApplicationInfo ApplicationInfo { get; set; }
        //public List<Item> Items { get; set; }
        public Preference Preferences { get; set; }
        //public string ExtraProducts { get; set; }
        //public string SellerRestriction { get; set; }
        //public string User { get; set; }
        public string SellerCode { get; set; }
        public string SellerPlaceCode { get; set; }
        public Consent Consent { get; set; }
    }
}