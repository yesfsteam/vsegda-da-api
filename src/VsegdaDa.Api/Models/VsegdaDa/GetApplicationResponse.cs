﻿using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.VsegdaDa
{
    public class GetApplicationResponse : JsonModel
    {
        public string ResultCode { get; set; }
        public ApplicationResponseData Data { get; set; }
    }
}