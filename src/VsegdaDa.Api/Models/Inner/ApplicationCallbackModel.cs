﻿using System;
using VsegdaDa.Api.Models.VsegdaDa.Enums;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.Inner
{
    public class ApplicationCallbackModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Номер заказа
        /// </summary>
        public string VsegdaDaOrderNum { get; set; }
        
        /// <summary>
        /// Статус заявки
        /// </summary>
        public VsegdaDaCreditApplicationStatus VsegdaDaStatus { get; set; }
        
        /// <summary>
        /// Решения по заявке, в формате Json
        /// </summary>
        public string VsegdaDaProposalsBody { get; set; }
    }
}