﻿using System;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.Inner
{
    public class ApplicationStatusModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Сумма заказа
        /// </summary>
        public decimal? VsegdaDaOrderSum { get; set; }
        
        /// <summary>
        /// Сумма первоначального взноса
        /// </summary>
        public decimal? VsegdaDaInitialPay { get; set; }
        
        /// <summary>
        /// Информация о созданной заявке, в формате Json
        /// </summary>
        public string VsegdaDaApplicationBody { get; set; }
        
        /// <summary>
        /// Решения по заявке, в формате Json
        /// </summary>
        public string VsegdaDaProposalsBody { get; set; }
    }
}