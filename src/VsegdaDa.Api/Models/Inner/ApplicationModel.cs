﻿using System;
using System.Net;
using VsegdaDa.Api.Models.VsegdaDa.Enums;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.Inner
{
    public class ApplicationModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Статус ответа ВД
        /// </summary>
        public string VsegdaDaResultCode { get; set; }
        
        /// <summary>
        /// Идентификатор запроса ВД
        /// </summary>
        public Guid? VsegdaDaApplicationId { get; set; }
        
        /// <summary>
        /// Информация о созданной заявке, в формате Json
        /// </summary>
        public string VsegdaDaApplicationBody { get; set; }
        
        /// <summary>
        /// URL для переадресации на страницу оформления заявки
        /// </summary>
        public string VsegdaDaBankUrl { get; set; }
        
        /// <summary>
        /// Номер заказа
        /// </summary>
        public string VsegdaDaOrderNum { get; set; }
        
        /// <summary>
        /// Статус заявки
        /// </summary>
        public VsegdaDaCreditApplicationStatus VsegdaDaStatus { get; set; }
        
        /// <summary>
        /// Решения по заявке, в формате Json
        /// </summary>
        public string VsegdaDaProposalsBody { get; set; }
    }
}