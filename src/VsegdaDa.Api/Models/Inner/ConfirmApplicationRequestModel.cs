﻿using System;
using VsegdaDa.Api.Models.VsegdaDa;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.Inner
{
    public class ConfirmApplicationRequestModel : JsonModel
    {
        public Guid AddplicationId { get; set; }
        public Guid GatewayId { get; set; }
        public ConfirmApplicationRequest Request { get; set; }
    }
}