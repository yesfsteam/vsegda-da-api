﻿using System;
using System.Net;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.Inner
{
    public class ApplicationConfirmationModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Идентификатор запроса ВД
        /// </summary>
        public Guid VsegdaDaApplicationId { get; set; }
        
        /// <summary>
        /// Идентификатор предложения ВД
        /// </summary>
        public Guid VsegdaDaProposalId { get; set; }
        
        /// <summary>
        /// Статус ответа ВД
        /// </summary>
        public string VsegdaDaResultCode { get; set; }
    }
}