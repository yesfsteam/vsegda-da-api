﻿using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;

namespace VsegdaDa.Api.Models.Inner
{
    public class CreditApplicationProposalStatusModel : CreditApplicationStatusModel
    {
        public Proposal Proposal { get; set; }
    }
}