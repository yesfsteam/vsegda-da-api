﻿namespace VsegdaDa.Api.Models
{
    public class Constants
    {
        public const string GRANT_TYPE = "CLIENT_CREDENTIALS";
        public const string CATEGORY = "CASH";
        public const string CHANNEL = "ONLINE";
        public const string ADDRESS_FORMAT = "s";
    }
}