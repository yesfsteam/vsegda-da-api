﻿using System;
using SD.Messaging.Models;
using VsegdaDa.Api.Models.VsegdaDa.Models;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.Commands
{
    public class SelectDecisionCommand : JsonModel, IRetryMessage
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Идентификатор кредитной организации
        /// </summary>
        public Guid CreditOrganizationId { get; set; }
        
        /// <summary>
        /// Предлоение от ВД
        /// </summary>
        public Proposal Proposal { get; set; }
        
        /// <summary>
        /// Идентификатор запроса ВД
        /// </summary>
        public Guid VsegdaDaApplicationId { get; set; }

        /// <summary>
        /// Количество попыток обработки
        /// </summary>
        public int RetryCount { get; set; }
    }
}