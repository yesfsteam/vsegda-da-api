﻿using System;
using SD.Messaging.Models;
using Yes.Infrastructure.Common.Models;

namespace VsegdaDa.Api.Models.Commands
{
    public class GetCreditApplicationStatusCommand : JsonModel, IRetryMessage
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Идентификатор запроса ВД
        /// </summary>
        public Guid VsegdaDaApplicationId { get; set; }

        /// <summary>
        /// Количество попыток обработки
        /// </summary>
        public int RetryCount { get; set; }
    }
}