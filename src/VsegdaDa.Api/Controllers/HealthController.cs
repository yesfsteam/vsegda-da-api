﻿using Microsoft.AspNetCore.Mvc;

namespace VsegdaDa.Api.Controllers
{
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        /// <summary>
        /// Проверка работоспособности приложения
        /// </summary>
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Ok");
        }
    }
}