﻿using System.Threading.Tasks;
using SD.Cqrs;
using SD.Messaging.Models;
using VsegdaDa.Api.Domain;
using VsegdaDa.Api.Models.Commands;

namespace VsegdaDa.Api.Messaging
{
    public class CreditApplicationCommandHandler : CommandHandler
    {
        private readonly ICreditApplicationManager manager;

        public CreditApplicationCommandHandler(ICreditApplicationManager manager) 
        {
            this.manager = manager; 
        }

        public async Task<ProcessingResult> Handle(GetCreditApplicationStatusCommand command)
            => await manager.GetCreditApplicationStatus(command);
        
        public async Task<ProcessingResult> Handle(SelectDecisionCommand command)
            => await manager.SelectDecision(command);
    }
}